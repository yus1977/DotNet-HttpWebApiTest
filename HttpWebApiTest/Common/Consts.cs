﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HttpWebApiTest
{
    /// <summary>
    /// 一些常用的常量
    /// </summary>
    public class Consts
    {
        public const string MediaTypeJson = "application/json";

        public const string HeaderUserAgent = "User-Agent";
        public const string HeaderAuthorization = "Authorization";
        public const string HeaderAppID = "AppID";
        public const string AuthorizationBearer = "Bearer";

    }
}
