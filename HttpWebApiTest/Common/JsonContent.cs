﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HttpWebApiTest.Common
{
    /// <summary>
    /// HttpCleint提交的内容 HttpContent,内容为Json
    /// </summary>
    public class JsonContent : StringContent
    {
        public JsonContent(object obj) : base(JsonConvert.SerializeObject(obj), Encoding.UTF8, Consts.MediaTypeJson)
        { }

        public JsonContent(string json) : base(json, Encoding.UTF8, Consts.MediaTypeJson)
        { }
    }
}
