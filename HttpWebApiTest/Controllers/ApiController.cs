﻿using HttpWebApiTest.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HttpWebApiTest.Controllers
{
    /// <summary>
    /// 用来测试的，进行远程访问的API
    /// </summary>
    [Route("api")]
    public class ApiController : ControllerBase
    {
        [HttpGet("user/{name}/{age}")]
        public InputMode GetUserInfo(string name, int age,
            [FromHeader] string appID,
            [FromHeader(Name= Consts.HeaderUserAgent)] string userAgent,
            [FromHeader] string authorization)
        {
            var ret = new InputMode(name, age);
            ret.State = $"get at {DateTime.Now}";

            //显示 HttpHead 信息
            ret.Tag = $"HttpHeads:: User-Agent:{userAgent},AppID:{appID},Authorization:{authorization}";

            return ret;

        }

        [HttpPost("setuser")]
        public InputMode SetUserByBody([FromBody]InputMode input)
        {
            input.State = $"set at {DateTime.Now}";

            return input;
        }

        [HttpPost("setuser2")]
        public InputMode SetUserByForm([FromForm]InputMode input)
        {
            input.AddedDay = DateTime.Now;
            input.State = $"set at {DateTime.Now}";

            return input;
        }
    }


}
