﻿using HttpWebApiTest.Common;
using HttpWebApiTest.Model;
using HttpWebApiTest.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace HttpWebApiTest.Controllers
{
    /// <summary>
    /// 使用HttpClient来访问远程Api
    /// </summary>
    [Route("http")]
    public class HttpClientController : ControllerBase
    {
        /// <summary>
        /// 注入HttpClientFactory
        /// </summary>
        /// <param name="httpClientFactory"></param>
        public HttpClientController(HttpClientService httpClientService,
            HttpClientService2 httpClientService2,
            HttpClientService3 httpClientService3)
        {
            this.httpClientService = httpClientService;
            this.httpClientService2 = httpClientService2;
            this.httpClientService3 = httpClientService3;
        }

        HttpClientService httpClientService;
        HttpClientService2 httpClientService2;
        HttpClientService3 httpClientService3;

        /// <summary>
        /// 从远API获取信息，方式1
        /// </summary>
        /// <returns></returns>
        [HttpGet("get1")]
        public string GetInfo1()
        {
            return httpClientService.GetInfoRemote();
        }

        /// <summary>
        /// 向远程服务器提交数据,方式1
        /// </summary>
        /// <returns></returns>
        [HttpGet("set1")]
        public string GetSetInfo1()
        {
            return httpClientService.SetInfoRemote();
        }

        /// <summary>
        /// 从远API获取信息,方式2
        /// </summary>
        /// <returns></returns>
        [HttpGet("get2")]
        public string GetInfo2()
        {
            return httpClientService2.GetInfoRemote();
        }

        /// <summary>
        /// 向远程服务器提交数据,方式2
        /// </summary>
        /// <returns></returns>
        [HttpGet("set2")]
        public string GetSetInfo2()
        {
            return httpClientService2.SetInfoRemote();
        }

        /// <summary>
        /// 从远API获取信息,方式3
        /// </summary>
        /// <returns></returns>
        [HttpGet("get3")]
        public string GetInfo3()
        {
            return httpClientService3.GetInfoRemote();
        }

        /// <summary>
        /// 向远程服务器提交数据,方式2
        /// </summary>
        /// <returns></returns>
        [HttpGet("set3")]
        public string GetSetInfo3()
        {
            return httpClientService3.SetInfoRemote();
        }
    }
}
