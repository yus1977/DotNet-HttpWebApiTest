﻿using HttpWebApiTest.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiClient;
using WebApiClient.Attributes;
using WebApiClient.Contexts;

namespace HttpWebApiTest.Controllers
{
    /// <summary>
    /// 过滤请求进行动态配置，每个请求之前进行动态配置
    /// </summary>
    public class MyApiFilterAttribute : ApiActionFilterAttribute
    {
        public override Task OnBeginRequestAsync(ApiActionContext context)
        {
            //获取请求头
            var header = context.RequestMessage.Headers;

            //动态加入JWT认证信息
            //string headerName = "Authorization";
            //jwt_token可以动态获取
            string jwtToken = "jwt_token";
            header.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(Consts.AuthorizationBearer, jwtToken);

/*            //客户端类别
            header.Add("User-Agent", "HttpClientFactory-Sample");
            //自定义的 客户端ID
            header.Add("AppID", "MyApp");*/

            return base.OnBeginRequestAsync(context);
        }
    }
}
