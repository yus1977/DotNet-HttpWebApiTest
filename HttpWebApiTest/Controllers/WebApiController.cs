﻿using HttpWebApiTest.Model;
using HttpWebApiTest.MyApi;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiClient;

namespace HttpWebApiTest.Controllers
{
    /// <summary>
    /// 使用WebApiClient组件来访问远程Api
    /// </summary>
    [Route("webapi")]
    public class WebApiController : ControllerBase
    {
        public WebApiController(IMyWebApi myWebApi)
        {
            //使用DI中注册的myWebApi
            this.myWebApi = myWebApi;

            //也可以使用
            //HttpApi.Register 或 HttpApi.Create 来动态的创建WebApi接口映射
        }

        IMyWebApi myWebApi;

        /// <summary>
        /// 从远API获取信息，同步方法
        /// </summary>
        /// <returns></returns>
        [HttpGet("get")]
        public InputMode GetInfo()
        {
            //使用DI中的IMyWebApi
            //同步方式调用异步方法
            return myWebApi.GetInfoRemoteAsync("yus",1112).GetAwaiter().GetResult();
        }

        /// <summary>
        /// 从远API获取信息，同步方法
        /// </summary>
        /// <returns></returns>
        [HttpGet("geta")]
        public async Task<InputMode> GetInfoAsync()
        {
            return await myWebApi.GetInfoRemoteAsync("yus", 1112);
        }

        /// <summary>
        /// 向远程服务器提交数据,JsonContent,异步方法
        /// </summary>
        /// <returns></returns>
        [HttpGet("set1")]
        public InputMode SetInfo1()
        {
            //同步方式调用异步方法
            return myWebApi.SetInfoRemoteAsync(InputMode.Build()).GetAwaiter().GetResult();
        }

        /// <summary>
        /// 向远程服务器提交数据,JsonContent,异步方法
        /// </summary>
        /// <returns></returns>
        [HttpGet("set1a")]
        public async Task<InputMode> SetInfo1Async()
        {
            //同步方式调用异步方法
            return await  myWebApi.SetInfoRemoteAsync(InputMode.Build());
        }

        /// <summary>
        /// 向远程服务器提交数据,FormContent,异步方法
        /// </summary>
        /// <returns></returns>
        [HttpGet("set2")]
        public InputMode SetInf2()
        {
            //同步方式调用异步方法
            return myWebApi.SetInfoRemoteAsync2(InputMode.Build()).GetAwaiter().GetResult();
        }

        /// <summary>
        /// 向远程服务器提交数据,FormContent,异步方法
        /// </summary>
        /// <returns></returns>
        [HttpGet("set2a")]
        public async Task<InputMode> SetInfo2Async()
        {
            //同步方式调用异步方法
            return await myWebApi.SetInfoRemoteAsync2(InputMode.Build());
        }
    }
}
