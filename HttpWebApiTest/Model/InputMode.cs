﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiClient.DataAnnotations;

namespace HttpWebApiTest.Model
{
    public class InputMode
    {
        public string Name { get; set; }
        public int Age { get; set; }

        [IgnoreWhenNull]
        public string State { get; set; } = "";

        [IgnoreWhenNull]
        [DateTimeFormat("yyyy-MM-dd")] //这个输出格式会被统一设置格式覆盖
        public DateTime? AddedDay { get; set; }

        [IgnoreWhenNull]
        public string Tag { get; set; } = "";

        public InputMode() { }

        public InputMode(string name, int age)
        {
            Name = name;
            Age = age;
            AddedDay = DateTime.Now;
        }

        /// <summary>
        /// 构造数据
        /// </summary>
        /// <returns></returns>
        public static InputMode Build()
        {
            return new InputMode("yus",42);
        }

    }
}
