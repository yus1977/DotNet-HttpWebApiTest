﻿using HttpWebApiTest.Controllers;
using HttpWebApiTest.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApiClient;
using WebApiClient.Attributes;

namespace HttpWebApiTest.MyApi
{
    /// <summary>
    /// 通过WebApiClient调用远程WebApi
    /// 定义远程WebApi接口，映射成本地接口
    /// 接口的服务器地址在注册时动态设置
    /// </summary>
/*    [Header("AppID", "MyApp")] //全局的请求头，也可以为具体的方法定义请求头
    [Header(HttpRequestHeader.UserAgent, "HttpClientFactory -Sample")]*/
    [MyApiFilter]
    public interface IMyWebApi : IHttpApi
    {
        /// <summary>
        /// GET api/user/{name}/{age}
        /// Return json
        /// </summary>
        /// <param name="name"></param>
        /// <param name="age"></param>
        /// <returns></returns>
        [HttpGet("api/user/{name}/{age}")]
        [Cache(10 * 1000)] //请求缓存10秒
        [JsonReturn]
        ITask<InputMode> GetInfoRemoteAsync(string name,int age);

        /// <summary>
        /// POST api/setuser 
        ///  JsonContent
        /// Return json
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("api/setuser")]
        [Timeout(10 * 1000)] // 10s超时
        [JsonReturn]
        ITask<InputMode> SetInfoRemoteAsync([JsonContent, Required] InputMode input);

        /// <summary>
        /// POST api/setuser2
        /// FormContent
        /// Return json
        /// </summary>
        [HttpPost("api/setuser2")]
        [JsonReturn]
        ITask<InputMode> SetInfoRemoteAsync2([FormContent, Required] InputMode input,
            [FormField] string nickName = "ysss",
            [FormField, Range(1, 100)] int? age = 1);

    }
}
