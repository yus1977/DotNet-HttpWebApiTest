﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiClient;
using WebApiClient.Attributes;

namespace HttpWebApiTest.MyApi
{
    /// <summary>
    /// 通过WebApiClient调用WX WebApi
    /// 定义远程WebApi接口，映射成本地接口
    /// </summary>
    [HttpHost("https://api.weixin.qq.com/")]
    public interface IWXWebApi : IHttpApi
    {
        /// <summary>
        /// WX的WebApi接口
        /// Get sns/jscode2session
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="secret"></param>
        /// <param name="js_code"></param>
        /// <param name="grant_type"></param>
        /// <returns></returns>
        [HttpGet("sns/jscode2session")]
        [JsonReturn]
        ITask<WXResultMsg> WXJscode2Session(string appid, string secret, string js_code, string grant_type = "authorization_code");
    }
}
