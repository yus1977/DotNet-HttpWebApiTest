﻿using HttpWebApiTest.Model;
using HttpWebApiTest.MyApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HttpWebApiTest.Services
{
    /// <summary>
    /// 方式3：类型化的使用方式可以预先把配置放到自定义的 HttpClient 中，然后在需要的地方都过依赖注入直接拿到 HttpClient 实例。
    /// </summary>
    public class HttpClientService3
    {
        /// <summary>
        /// 注入HttpClientFactory
        /// </summary>
        /// <param name="httpClientFactory"></param>
        public HttpClientService3(HttpClientMyApi myApiClient)
        {
            this.myApiClient = myApiClient;
        }

        HttpClientMyApi myApiClient;

        /// <summary>
        /// 从远API获取用户信息
        /// get http://localhost:5000/api/user/yus/12
        /// </summary>
        /// <returns></returns>
        public string GetInfoRemote()
        {
            return myApiClient.GetInfoRemoteAsync().Result;
        }

        /// <summary>
        /// 向远程服务器提交数据
        /// post http://localhost:5000/api/setuser
        /// </summary>
        /// <returns></returns>
        public string SetInfoRemote()
        {
            var response = myApiClient.SetInfoRemoteAsync(InputMode.Build()).Result;
            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsStringAsync().Result;
            }
            else
            {
                return response.ToString();
            }
        }

    }
}

