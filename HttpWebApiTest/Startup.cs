using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using HttpWebApiTest.Common;
using HttpWebApiTest.MyApi;
using HttpWebApiTest.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebApiClient.Extensions.HttpClientFactory;

namespace HttpWebApiTest
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            #region HttpClientFactory 相关
            //注册HttpClient工厂服务
            //在使用的地方，获得 IHttpClientFactory，然后通过它来创建 HttpClient 对象

            //方式1：简单模式
            //这种使用方式适合一次性的 HTTP 请求调用。
            //弊端是如果多次都要请求某站点的接口（比如：github），那就得写很多重复代码配置 HttpClient。
            services.AddHttpClient();

            //方式2：命名式使用方式
            //在注册时做好配置，使用时不就用再配置了,比如：
            //var client = _httpClientFactory.CreateClient("myapi");
            services.AddHttpClient("myapi", client =>
            {
                //请求的根地址
                client.BaseAddress = new Uri("http://localhost:5000");
                //设置请求头信息
                //客户端类别
                client.DefaultRequestHeaders.Add(Consts.HeaderUserAgent, "HttpClientFactory-Sample");
                //JWT认证信息
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Consts.AuthorizationBearer, "jwt token");
                //自定义的 客户端ID
                client.DefaultRequestHeaders.Add(Consts.HeaderAppID, "MyApp");
            });

            //方式3：类型化的使用方式可以预先把配置放到自定义的 HttpClient 中，然后在需要的地方都过依赖注入直接拿到 HttpClient 实例。
            services.AddTransient(typeof(HttpClientMyApi));            

            //注册Service对象
            services.AddTransient(typeof(HttpClientService));
            services.AddTransient(typeof(HttpClientService2));
            services.AddTransient(typeof(HttpClientService3));

            #endregion

            #region WebApiClient 相关
            //注册接口映射，使用HttpClientFactory
            services.AddHttpApiTypedClient<IMyWebApi>(c =>
            {
                c.HttpHost = new Uri("http://localhost:5000/");
                //统一设置日期输出格式
                //会覆盖返回的Model中设置的格式
                c.FormatOptions.DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff";

                //统一设置HttpClient的默认请求头
                //客户端类别
                c.HttpClient.DefaultRequestHeaders.Add(Consts.HeaderUserAgent, "HttpClientFactory-Sample");
                //自定义的 客户端ID
                c.HttpClient.DefaultRequestHeaders.Add(Consts.HeaderAppID, "MyApp");
            });
            services.AddHttpApiTypedClient<IWXWebApi>();

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
