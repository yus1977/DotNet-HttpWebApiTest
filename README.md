# HttpClient和WebApiClient使用Demo
 1. .NetCore内置的HttpClient使用
 2. 开源组件WebApiClient使用

## 1 HttpClient使用
使用说明:
  * https://www.cnblogs.com/willick/p/net-core-httpclient.html

注意，创建的HttpClient默认使用的HttpClientHandler（HttpMessageHandler）会保存2分钟的会话，同时提交时会自动带HttpClent缓存的Cookies
可以使用下面的代码在HttpClient注入时配置 HttpClientHandler
``` c#
            //默认的HttpMessageHandler会自动保持2分钟会话和cookies,SSO应用不需要保持Cookies
            .ConfigurePrimaryHttpMessageHandler(h => {
                var handler = new HttpClientHandler();
                handler.UseDefaultCredentials = false;
                handler.UseCookies = false;
                return handler;
            });
```

## 2 WebApiClient使用
一个开源项目，基于HttpClient封装，只需要定义c#接口并修饰相关特性，即可异步调用远程http接口的客户端库
源代码和使用说明:
* https://github.com/dotnetcore/WebApiClient

### 需要安装包有：
```c#
//WebApiClient 基本包
install-package WebApiClient.JIT
//HttpClientFactory扩展
install-package WebApiClient.Extensions.HttpClientFactory
//或 DependencyInjection扩展
install-package WebApiClient.Extensions.DependencyInjection

```

### 主要使用步骤：
1. 声明接口，把远程Url映射成本地可以使用的接口  
   参看MyApi/IMyWebApi

2. 注册或创建接中映射和初始化配置  
  > 方式1：在启用时注册到DI中,比如
   
```c#
            services.AddHttpApiTypedClient<IMyWebApi>(c =>
            {
                c.HttpHost = new Uri("http://localhost:5000/");
                //统一设置日期输出格式
                //会覆盖返回的Model中设置的格式
                c.FormatOptions.DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff";

                //统一设置HttpClient的默认请求头
                //客户端类别
                c.HttpClient.DefaultRequestHeaders.Add(Consts.HeaderUserAgent, "HttpClientFactory-Sample");
                //自定义的 客户端ID
                c.HttpClient.DefaultRequestHeaders.Add(Consts.HeaderAppID, "MyApp");
            })
            //默认的HttpMessageHandler会自动保持2分钟会话和cookies
            //SSO应用不需要保持Cookies，所以如下配置
            .ConfigurePrimaryHttpMessageHandler(h => {
                var handler = new HttpClientHandler();
                handler.UseDefaultCredentials = false;
                handler.UseCookies = false;
                return handler;
            });

```   

   > 方式2：使用HttpApi.Register 或 HttpApi.Create 来动态的创建WebApi接口映射
比如:
``` c#
            //  注册IUserApi 配置其工厂
            HttpApi.Register<IUserApi>().ConfigureHttpApiConfig(c =>
            {
                c.HttpHost = new Uri("http://localhost:9999/");
                c.FormatOptions.DateTimeFormat = DateTimeFormats.ISO8601_WithMillisecond;
            });

           //使用
           var userApi = HttpApi.Resolve<IUserApi>();
```
3. 直接调用接口定义的方法，WebApiClient映射成访问远程的Url